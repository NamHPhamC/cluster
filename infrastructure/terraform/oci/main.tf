terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "4.92.0"
    }
  }
}

provider "oci" {
  region = "ap-sydney-1"
}

provider "oci" {
  alias  = "sydney"
  region = "ap-sydney-1"
}
