resource "oci_containerengine_cluster" "arcturus" {
  compartment_id     = oci_identity_compartment.tf_compartment.id
  kubernetes_version = "v1.24.1"
  name               = "arcturus"
  vcn_id             = oci_core_vcn.vcn.id

  endpoint_config {
    is_public_ip_enabled = true
    subnet_id            = oci_core_subnet.api_subnet.id
  }

  options {
    service_lb_subnet_ids = [oci_core_subnet.public_subnet.id]
  }
}

data "oci_core_images" "i" {
  compartment_id           = oci_identity_compartment.tf_compartment.id
  operating_system         = "Oracle Linux"
  operating_system_version = "7.9"
  shape                    = "VM.Standard.A1.Flex"

  filter {
    name   = "display_name"
    values = ["^.*Oracle[^G]*$"]
    regex  = true
  }
}

resource "oci_containerengine_node_pool" "ARM" {
  cluster_id         = oci_containerengine_cluster.arcturus.id
  compartment_id     = oci_identity_compartment.tf_compartment.id
  kubernetes_version = "v1.24.1"
  name               = "arcturus-pool-ARM"
  node_shape         = "VM.Standard.A1.Flex"
  node_config_details {
    placement_configs {
      availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
      subnet_id           = oci_core_subnet.private_subnet.id
    }

    size = 1
  }

  node_shape_config {
    memory_in_gbs = 24
    ocpus         = 4
  }

  node_source_details {
    image_id    = data.oci_core_images.i.images[0].id
    source_type = "IMAGE"
  }
}
