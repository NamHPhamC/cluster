resource "oci_file_storage_mount_target" "data" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  compartment_id      = oci_identity_compartment.tf_compartment.id
  subnet_id           = oci_core_subnet.private_subnet.id
}

resource "oci_file_storage_file_system" "data" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  compartment_id      = oci_identity_compartment.tf_compartment.id

  display_name = "arcturus-data"
}

resource "oci_file_storage_export" "data" {
  export_set_id = oci_file_storage_mount_target.data.export_set_id

  file_system_id = oci_file_storage_file_system.data.id
  path           = "/"

  export_options {
    source          = "0.0.0.0/0"
    access          = "READ_WRITE"
    identity_squash = "NONE"
  }
}
