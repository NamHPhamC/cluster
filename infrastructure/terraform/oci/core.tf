resource "oci_identity_compartment" "tf_compartment" {
  provider = oci.sydney

  compartment_id = var.tenancy_ocid
  description    = "Compartment for Terraform resources."
  name           = "terraform"
}
