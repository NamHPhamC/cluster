resource "oci_core_vcn" "vcn" {
  cidr_block     = "10.1.0.0/16"
  compartment_id = oci_identity_compartment.tf_compartment.id
  display_name   = "vcn-arcturus"
  dns_label      = "vcnarcturus"
}

resource "oci_core_internet_gateway" "internet_gateway" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  display_name   = "igw-arcturus"
  vcn_id         = oci_core_vcn.vcn.id
}

resource "oci_core_nat_gateway" "nat_gateway" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  display_name   = "ngw-arcturus"
  vcn_id         = oci_core_vcn.vcn.id
}

resource "oci_core_default_route_table" "public_route_table" {
  manage_default_resource_id = oci_core_vcn.vcn.default_route_table_id

  compartment_id = oci_identity_compartment.tf_compartment.id
  display_name   = "rt-public-arcturus"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.internet_gateway.id
  }
}

resource "oci_core_route_table" "private_route_table" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  display_name   = "rt-private-arcturus"
  vcn_id         = oci_core_vcn.vcn.id

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_nat_gateway.nat_gateway.id
  }

  route_rules {
    destination       = lookup(data.oci_core_services.all_oci_services.services.0, "cidr_block")
    destination_type  = "SERVICE_CIDR_BLOCK"
    network_entity_id = oci_core_service_gateway.service_gateway.id
  }
}

data "oci_core_services" "all_oci_services" {
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
}

resource "oci_core_service_gateway" "service_gateway" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  services {
    service_id = lookup(data.oci_core_services.all_oci_services.services.0, "id")
  }
  vcn_id = oci_core_vcn.vcn.id

  display_name = "sg-arcturus"
}

resource "oci_core_subnet" "api_subnet" {
  cidr_block   = "10.1.1.0/28"
  display_name = "apisubnet-arcturus"
  security_list_ids = [
    oci_core_security_list.api_security_list.id
  ]
  compartment_id  = oci_identity_compartment.tf_compartment.id
  vcn_id          = oci_core_vcn.vcn.id
  route_table_id  = oci_core_default_route_table.public_route_table.id
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

resource "oci_core_security_list" "api_security_list" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  vcn_id         = oci_core_vcn.vcn.id
  display_name   = "api-seclist-arcturus"

  ingress_security_rules {
    protocol = "6"
    source   = "203.123.122.165/32"
    tcp_options {
      min = 6443
      max = 6443
    }
  }

  ingress_security_rules {
    protocol = "6"
    source   = "10.1.2.0/24"
    tcp_options {
      min = 6443
      max = 6443
    }
  }

  ingress_security_rules {
    protocol = "6"
    source   = "10.1.2.0/24"
    tcp_options {
      min = 12250
      max = 12250
    }
  }

  ingress_security_rules {
    protocol = "1"
    source   = "10.1.2.0/24"
    icmp_options {
      code = 4
      type = 3
    }
  }

  egress_security_rules {
    protocol    = "1"
    destination = "10.1.2.0/24"
    icmp_options {
      code = 4
      type = 3
    }
  }

  egress_security_rules {
    protocol    = "6"
    destination = "10.1.2.0/24"
  }

  egress_security_rules {
    protocol         = "6"
    destination      = lookup(data.oci_core_services.all_oci_services.services.0, "cidr_block")
    destination_type = "SERVICE_CIDR_BLOCK"
    tcp_options {
      min = 443
      max = 443
    }
  }
}

resource "oci_core_subnet" "private_subnet" {
  availability_domain        = data.oci_identity_availability_domains.ads.availability_domains[0].name
  cidr_block                 = "10.1.2.0/24"
  display_name               = "private-subnet-arcturus"
  prohibit_public_ip_on_vnic = true
  security_list_ids = [
    oci_core_security_list.private_security_list.id
  ]
  compartment_id  = oci_identity_compartment.tf_compartment.id
  vcn_id          = oci_core_vcn.vcn.id
  route_table_id  = oci_core_route_table.private_route_table.id
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

resource "oci_core_security_list" "private_security_list" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  vcn_id         = oci_core_vcn.vcn.id
  display_name   = "private-seclist-arcturus"
  lifecycle {
    ignore_changes = [
      ingress_security_rules, // We need this to avoid nuking changes made by the OKE operator
      egress_security_rules,  // We need this to avoid nuking changes made by the OKE operator
    ]
  }

  ingress_security_rules {
    protocol = "all"
    source   = "10.1.2.0/24"
  }

  ingress_security_rules {
    source   = "10.1.1.0/28"
    protocol = "1"
    icmp_options {
      code = 4
      type = 3
    }
  }

  ingress_security_rules {
    source   = "10.1.1.0/28"
    protocol = "6"
  }

  ingress_security_rules {
    source   = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
      min = 22
      max = 22
    }
  }

  egress_security_rules {
    protocol    = "all"
    destination = "10.1.2.0/24"
  }

  egress_security_rules {
    protocol    = "6"
    destination = "10.1.1.0/28"
    tcp_options {
      min = 6443
      max = 6443
    }
  }

  egress_security_rules {
    protocol    = "6"
    destination = "10.1.1.0/28"
    tcp_options {
      min = 12250
      max = 12250
    }
  }

  egress_security_rules {
    protocol    = "1"
    destination = "10.1.1.0/28"
    icmp_options {
      code = 4
      type = 3
    }
  }

  egress_security_rules {
    protocol         = "6"
    destination      = lookup(data.oci_core_services.all_oci_services.services.0, "cidr_block")
    destination_type = "SERVICE_CIDR_BLOCK"
    tcp_options {
      min = 443
      max = 443
    }
  }

  egress_security_rules {
    protocol    = "1"
    destination = "0.0.0.0/0"
    icmp_options {
      code = 4
      type = 3
    }
  }

  egress_security_rules {
    protocol    = "all"
    destination = "0.0.0.0/0"
  }


}

resource "oci_core_subnet" "public_subnet" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  cidr_block          = "10.1.3.0/24"
  display_name        = "public-subnet-arcturus"
  security_list_ids = [
    oci_core_security_list.public_security_list.id
  ]
  compartment_id  = oci_identity_compartment.tf_compartment.id
  vcn_id          = oci_core_vcn.vcn.id
  route_table_id  = oci_core_default_route_table.public_route_table.id
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

resource "oci_core_security_list" "public_security_list" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  vcn_id         = oci_core_vcn.vcn.id
  display_name   = "public-seclist-arcturus"

  lifecycle {
    ignore_changes = [
      ingress_security_rules, // We need this to avoid nuking changes made by the OKE operator
      egress_security_rules,  // We need this to avoid nuking changes made by the OKE operator
    ]
  }
}
