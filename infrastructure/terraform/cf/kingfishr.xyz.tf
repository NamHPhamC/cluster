resource "cloudflare_zone" "kingfishr-xyz" {
  zone = "kingfishr.xyz"
  plan = "free"
}

resource "cloudflare_record" "vpn-kingfishr-xyz" {
  for_each = var.vpn_peers
  zone_id  = cloudflare_zone.kingfishr-xyz.id
  name     = "${each.key}.vpn"
  value    = each.value.ip
  type     = "A"
  proxied  = false
}
