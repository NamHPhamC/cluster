# Arcturus Terraform Infrastructure

Terraformed infrastructure for the Arcturus lab.

## :wrench:&nbsp; Tools

The following tools are required for taking full advantage of this repo.

| Tool                                                               | Purpose
|--------------------------------------------------------------------|---------------------------------------------------------------------|
| [terraform](https://terraform.io)                                  | Allows us to provision our infrastructure, including Cloudflare, Oracle cloud and Zerotier       |
| [VSCode SOPS](https://marketplace.visualstudio.com/items?itemName=signageos.signageos-vscode-sops) | Automatically decrypts/encrypts SOPS encrypted secrets for editing |

## Terraform

### Initalising

```sh
PROJECT_ID="30896904"
TF_USERNAME="<ARCTURUS USER>"
TF_PASSWORD="<API_TOKEN>"
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/state"

terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
```
