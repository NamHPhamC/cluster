variable "vpn_peers" {
  default = {
    arcturus = {
      ip       = "10.42.24.96",
      peer_id  = "714aea4352",
      bridging = true,
    },
    pulsar = {
      ip       = "10.42.24.24",
      peer_id  = "5ef485b350",
      bridging = false,
    },
    europa = {
      ip       = "10.42.24.69",
      peer_id  = "d51fe9ac28",
      bridging = false,
    },
    janus = {
      ip       = "10.42.24.42",
      peer_id  = "078836e346",
      bridging = false,
    },
    io = {
      ip       = "10.42.24.134",
      peer_id  = "edeed091ff",
      bridging = false,
    },
    phone1 = {
      ip       = "10.42.24.135",
      peer_id  = "8cd86fcdea",
      bridging = false,
    },
    pallas = {
      ip       = "10.42.24.43",
      peer_id  = "fecb57e592",
      bridging = true,
    },
    mimas = {
      ip       = "10.42.24.136",
      peer_id  = "5bc70571d6",
      bridging = false,
    },
    pandora = {
      ip       = "10.42.24.137",
      peer_id  = "73870b6bec",
      bridging = false,
    }
  }
}

variable "tenancy_ocid" {}
