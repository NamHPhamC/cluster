terraform {
  required_providers {
    zerotier = {
      source  = "zerotier/zerotier"
      version = "1.2.0"
    }
  }
}
