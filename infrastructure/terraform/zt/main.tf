data "local_file" "flow_rules" {
  filename = "${path.module}/flow_rules"
}

resource "zerotier_network" "kingfishr" {
  name             = "kingfishr"
  description      = ""
  private          = true
  enable_broadcast = true
  assign_ipv4 {
    zerotier = true
  }
  assign_ipv6 {
    rfc4193  = false
    sixplane = false
    zerotier = false
  }
  assignment_pool {
    start = "10.42.24.1"
    end   = "10.42.24.199"
  }
  route {
    target = "10.42.24.0/24"
  }
  route {
    target = "192.168.1.0/24"
    via    = "10.42.24.24"
  }
  route {
    target = "192.168.2.0/24"
    via    = "10.42.24.43"
  }
  flow_rules      = data.local_file.flow_rules.content
  multicast_limit = 256
}

resource "zerotier_member" "peers" {
  for_each                = var.vpn_peers
  name                    = each.key
  description             = ""
  member_id               = each.value.peer_id
  network_id              = zerotier_network.kingfishr.id
  hidden                  = false
  allow_ethernet_bridging = each.value.bridging
  no_auto_assign_ips      = false
  ip_assignments          = ["${each.value.ip}"]
}
