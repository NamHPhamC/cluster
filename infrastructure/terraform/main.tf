module "cf" {
  source = "./cf"

  vpn_peers = var.vpn_peers
}

module "zt" {
  source = "./zt"

  vpn_peers = var.vpn_peers
}

module "oci" {
  source = "./oci"

  tenancy_ocid = var.tenancy_ocid
}
