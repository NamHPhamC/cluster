# Arcturus Kubernetes Cluster

## :wrench:&nbsp; Tools

The following tools are required for taking full advantage of this repo.

| Tool                                                               | Purpose
|--------------------------------------------------------------------|---------------------------------------------------------------------|
| [flux](https://toolkit.fluxcd.io/)                                 | Operator that manages your k8s cluster based on your Git repository |
| [SOPS](https://github.com/mozilla/sops)                            | Encrypts k8s secrets with GnuPG                                     |
| [pre-commit](https://github.com/pre-commit/pre-commit)             | Runs checks during `git commit`                                     |
| [kustomize](https://kustomize.io/)                                 | Template-free way to customize application configuration            |
| [helm](https://helm.sh/)                                           | Manage Kubernetes applications                                      |
| [VSCode SOPS](https://marketplace.visualstudio.com/items?itemName=signageos.signageos-vscode-sops) | Automatically decrypts/encrypts SOPS encrypted secrets for editing |
| [age](https://github.com/FiloSottile/age)                          | A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config options, and UNIX-style composability. |

### pre-commit

After pre-commit is installed on your system, run:

```sh
pre-commit install --install-hooks
```

## Flux

1. Verify Flux can be installed

```sh
flux check --pre
```

2. Pre-create the `flux-system` namespace

```sh
kubectl create namespace flux-system --dry-run=client -o yaml | kubectl apply -f -
```

3. Add the Flux age key in-order for Flux to decrypt SOPS secrets

```sh
cat ~/.config/sops/age/keys.txt |
kubectl create secret generic sops-age \
    --namespace=flux-system \
    --from-file=age.agekey=/dev/stdin
```

4. Add the gitlab credentials to access the private repo, `<gitlab-token>` needs to have `repo:read` permissions

```sh
flux create secret git flux-system \
    --url="https://gitlab.com/arcturus-lab/cluster.git" \
    --export \
    --username=git \
    --password="<gitlab token>" | kubectl apply -f -
```

5. Install Flux

The following needs to be run twice to correctly use the newly created flux CRDs

```sh
kubectl apply --kustomize=./cluster/base/flux-system
```

## Appendix

### Age key

Setup your Age key with the following
```
mkdir -p ~/.config/sops/age
age-keygen -o ~/.config/sops/age/keys.txt
```

## Thanks

A big thanks to the folks over at k8s-at-home, who's [template-cluster-k3s](https://github.com/k8s-at-home/template-cluster-k3s) this was originally based on.
